var express = require("express");
var mongoose = require("./db");
var cors = require("cors");
var app = express();

app.use(cors());
app.use(express.json());

app.use(require('./routers'));

app.use(express.urlencoded({
    extended: true
}));


//Comando para indicar indicar condificacion de externo
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Authorization",
        "X-API-KEY",
        "Origin",
        "X-Requested-With",
        "Content-Type, Accept",
        "Access-Control-Allow-Request-Method"
    );
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
    next();
});

app.listen(5000);

module.exports = app; 
module.exports = mongoose;