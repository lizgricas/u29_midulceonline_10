const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const proveedorSchema = new Schema(
    {
        nombre: {
            type: String,
            required: true,
        },
        tipoDoc: {
            type: String,
        },
        numDoc: {
            type: String,
            required: true,
        },
        direccion: {
            type: String,
            required: true,
        },
        telefono: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
        }
    }
    , {
        collection: 'proveedores',
        versionKey: false
    }
);


var Proveedor = mongoose.model('proveedores', proveedorSchema);
module.exports = Proveedor