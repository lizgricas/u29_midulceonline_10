
const Usuario = require("../models/Usuario");

function login(req, res) {
    let usuario = req.body;
    console.log("login" + usuario.Usuario);
    usuario = Usuario.find({
        usuario: usuario.Usuario,
        clave: usuario.Clave,
    });
    res.json(usuario);
}


function all(req, res) {
    Usuario.find()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err })
        });
}

function search(req, res) {
    Usuario.find()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err })
        });
}

function save(req, res) {

    const usuario = new Usuario({ //objeto usuario
        usuario: req.body.usuario,
        clave: req.body.clave,
        rol: req.body.rol,
        nombres: req.body.nombres,
        apellidos: req.body.apellidos,
        tipDoc: req.body.tipDoc,
        numDoc: req.body.numDoc,
        direccion: req.body.direccion,
        telefono: req.body.telefono,
        email: req.body.email,
        estado: req.body.estado,
    });

    usuario.save()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err })
        });

}

function destroy(req, res) {
    Usuario.deleteOne({ _id: req.params.id })
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err });
        });
}

function update(req, res) {
    let hopa = req.params;
    Usuario.updateOne(hopa._id, hopa)
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err });
        });
}

module.exports = {
    all,
    save,
    destroy,
    search,
    login,
    update
}
