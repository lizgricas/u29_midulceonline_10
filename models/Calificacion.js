const mongoose = require("mongoose");
const Schema = mongoose.Schema;
//este archivo define las tabñes (colecciones de base de datos no relacionales), estructura de las tablas
let calificacionSchema = new Schema(
    {
        idProducto: {
            type: Number,
            required: true,
        },
        descripcion: {
            type: String,
            required: true,
        },
        idUsuario: {
            type: Number,
            required: true,
        },
        nota: {
            type: Number,
        }
    }
    , {
        collection: 'calificaciones',
        versionKey: false
    }
);

var Calificacione = mongoose.model('calificaciones', calificacionSchema);
module.exports = Calificacione

