const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const configuracionSchema = new Schema(
    {
        valorEnvio: {
            type: Number,
            required: true,
        },
        topeEnvio: {
            type: Number,
            required: true,
        },
        nombreTienda: {
            type: String,
            required: true,
        }
    }
    , {
        collection: 'configuraciones',
        versionKey: false
    }
);



var Configuracion = mongoose.model('configuraciones', configuracionSchema);
module.exports = Configuracion
