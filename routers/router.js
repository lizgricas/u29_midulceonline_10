const { Router } = require('express'); // express es la dependencia que nos va a permitir definir rutas de navegacion en el navegador backend
const router = Router();
export default router;

// el archivo router especifica los endpoints del backend

module.exports = router;