const Producto = require("../models/Producto");

function all(req, res) {
    Producto.find()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err })
        });
}

function search(req, res) {
    Producto.find()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err })
        });
}

function save(req, res) {

    const producto = new Producto({
        nombre: req.body.nombre,
        codigo: req.body.codigo,
        categoria: req.body.categoria,
        imagen: req.body.imagen,
        unidadMedida: req.body.unidadMedida,
        valorCompra: req.body.valorCompra,
        valorVenta: req.body.valorVenta,
        cantidad: req.body.cantidad,
        calificacion: req.body.calificacion,    
    });

    producto.save()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err })
        });

}

function destroy(req, res) {
    Producto.deleteOne({ _id: req.params.id })
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err });
        });
}

function update(req, res) {
    let hopa = req.params;
    Producto.updateOne(hopa._id, hopa)
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err });
        });
}

module.exports = {
    all,
    save,
    destroy,
    search,
    update
}