const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const usuarioSchema = new Schema(
    {
        usuario: {
            type: String,
            required: true,
            unique: true
        },
        clave: {
            type: String,
            required: true,
        },
        rol: {
            type: String,
            required: true,
        },
        nombres: {
            type: String,
            required: true,
        },
        apellidos: {
            type: String,
            required: true,
        },
        tipDoc: {
            type: String,
            required: true,
        },
        numDoc: {
            type: String,
            required: true,
        },
        direccion: {
            type: String,
            required: true,
        },
        telefono: {
            type: String,
            required: true,
        },
        email: {
            type: String,
            required: true,
        },
        estado: {
            type: String,
            required: true,
        }
    }
    , {
        collection: 'usuarios',
        versionKey: false
    }
);


var Usuario = mongoose.model('usuarios', usuarioSchema);
module.exports = Usuario