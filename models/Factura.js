const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const facturaSchema = new Schema(
    {
        codigoFactura: {
            type: Number,
            required: true,
        },
        fecha: {
            type: Date,
            required: true,
        },
        comprador: {
            type: Number,
            required: true,
            unique: true
        },
        vendedor: {
            type: String,
            required: true,
        },
        estadoFactura: {
            type: Number,
            required: true,
        },
        tipoFactura: {
            type: Number,
            required: true,
        },
        valor: {
            type: Number,
            required: true,
        },
        detalle: [
            {
                type: String,
                required: true
            }
        ],
        fechaPreparacion: {
            type: Date,
            required: true
        },
        fechaEnvio: {
            type: Date,
            required: true
        },
        fechaEntrega: {
            type: Date,
            required: true
        }
    }
    , {
        collection: 'facturas',
        versionKey: false
    }
);


var Factura = mongoose.model('facturas', facturaSchema);
module.exports = Factura