const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const productoSchema = new Schema({
    nombre: {
        type: String,
        required: true,
        unique: true
    },
    codigo: {
        type: String,
        required: true,
        unique: true
    },
    categoria: {
        type: String,
        required: true
    },
    imagen: {
        type: String,
    },
    unidadMedida: {
        type: String,
        required: true
    },
    valorCompra: {
        type: Number,
        required: true
    },
    valorVenta: {
        type: Number,
        required: true
    },
    cantidad: {
        type: Number,
        required: true
    },
    calificacion: {
        type: Number
    }
}
    , {
        collection: 'productos',
        versionKey: false
    }
);



var Producto = mongoose.model('productos', productoSchema);
module.exports = Producto