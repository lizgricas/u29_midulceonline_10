const Proveedor = require("../models/Proveedor");

function all(req, res) {
    Proveedor.find()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err })
        });
}

function search(req, res) {
    Producto.find()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err })
        });
}

function save(req, res) {

    const proveedor = new Proveedor({ //objeto usuario
        nombre: req.body.nombre,
        tipoDoc: req.body.tipoDoc,
        numDoc: req.body.numDoc,
        direccion: req.body.direccion,
        telefono: req.body.telefono,
        email: req.body.email,
    });

    proveedor.save()
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err })
        });

}

function destroy(req, res) {
    Proveedor.deleteOne({ _id: req.params.id })
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err });
        });
}

function update(req, res) {
    let hopa = req.params;
    Proveedor.updateOne(hopa._id, hopa)
        .then((data) => {
            res.json(data);
        })
        .catch((err) => {
            res.json({ message: err });
        });
}

module.exports = {
    all,
    save,
    destroy,
    search,
    update
}