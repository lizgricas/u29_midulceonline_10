let express = require('express'); //Llamado a la dependencia express
let router = express.Router(); //Creación del router usando express

const ctrlUsuarios = require("./controller/UsuariosCtrl");
const ctrlProductos = require("./controller/ProductosCtrl");
const ctrlProveedores = require("./controller/ProveedoresCtrl");

router.post("/productos/save", ctrlProductos.save);
router.get("/productos/all", ctrlProductos.all);
router.get("/productos/search/:id", ctrlProductos.search);
router.put("/productos/update/:id", ctrlProductos.update);
router.delete("/productos/destroy/:id", ctrlProductos.destroy);

router.post("/usuarios/login", ctrlUsuarios.login);
router.post("/usuarios/save", ctrlUsuarios.save);
router.get("/usuarios/all", ctrlUsuarios.all);
router.get("/usuarios/search/:id", ctrlUsuarios.search);
router.put("/usuarios/update/:id", ctrlUsuarios.update);
router.delete("/usuarios/destroy/:id", ctrlUsuarios.destroy);

router.post("/proveedores/save", ctrlProveedores.save);
router.get("/proveedores/all", ctrlProveedores.all);
router.get("/proveedores/search/:id", ctrlProveedores.search);
router.put("/proveedores/update/:id", ctrlProveedores.update);
router.delete("/proveedores/destroy/:id", ctrlProveedores.destroy);

module.exports = router; //Exportación de las rutas 
